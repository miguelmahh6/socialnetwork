import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { PeopleService } from './services/people.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TPSOCIAL';

  constructor(public peopleService: PeopleService,public auth : AuthService) {

    this.peopleService.informationUser();
  }
}
