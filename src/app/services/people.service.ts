import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegistrationModel } from '../models/registration.model';
import { AuthService } from './auth.service';


@Injectable({
    providedIn: 'root'
})
export class PeopleService {

    private url = 'https://tpbd-80184-default-rtdb.firebaseio.com/';
    private apikey = 'AIzaSyCmflSZNaWCkpR1bn36q5zkeI2W4b5jhjY';

    userToken!: string;

    userInSession = new RegistrationModel;
    allUsers: Array<RegistrationModel> = [];

    constructor(private http: HttpClient, private auth: AuthService) {
        this.leerToken();
    }


    /* getHeaders() {
        const headers = new HttpHeaders({
            'Authorization': 'Basic useridfromauth'
        });
        return headers;
    } */


    editUser(index: number, modifiedUser: any) {
        var url = this.url + "people/" + index + ".json";
        return this.http.put(url, modifiedUser);
    }

    newUser(request: Array<RegistrationModel>) {
        return this.http.put(`${this.url + "people.json"}`, request);
        //var headers=this.headers;
        //  return this.http.post(`${this.url}`, request, { headers });
        //return this.http.post(`${this.url}`, request);

    }

    getAll() {
        return this.http.get(`${this.url + "people.json"}`);
    }


    private guardarToken(idToken: string) {

        this.userToken = idToken;
        localStorage.setItem('token', idToken);

        let hoy = new Date();
        hoy.setSeconds(3600);

        localStorage.setItem('expira', hoy.getTime().toString());


    }

    leerToken() {

        if (localStorage.getItem('token')) {
            this.userToken = localStorage.getItem('token')!;
        } else {
            this.userToken = '';
        }

        return this.userToken;

    }

    getProfileByIdSession() {
        for (let index = 0; index < this.allUsers.length; index++) {
            const element = this.allUsers[index];
            if (element.systemId == this.auth.decodeJWT().user_id) {
                this.userInSession = element;
                break;
            }
        }
        localStorage.setItem("InformationUser", btoa(JSON.stringify(this.userInSession)));

        return this.userInSession
    }

    //used to find any person
    getProfileBySystemId(request: string) {
        var infoUserFound: RegistrationModel = new RegistrationModel
        for (let index = 0; index < this.allUsers.length; index++) {
            const element = this.allUsers[index];
            if (element.systemId == request) {
                infoUserFound = element;
                break;
            }
        }

        return infoUserFound
    }

    clearSession() {
        this.getEmptyUser();
        this.allUsers = [];
    }

    informationUser() {

        try {
            if (localStorage.getItem("InformationUser")) {
                this.userInSession = JSON.parse(atob(localStorage.getItem("InformationUser") || '{}'));
            }
            return this.userInSession
        } catch (error) {
            return this.getEmptyUser();
        }
    }


    getEmptyUser() {
        this.userInSession = {
            email: "",
            bornDate: "",
            aboutMe: "",
            firstName: "",
            firstLastName: "",
            id: "",
            inRelationship: false,
            profileImg: "",
            secondName: "",
            secondLastName: "",
            systemId: "",

        }


        return this.userInSession
    }
}
