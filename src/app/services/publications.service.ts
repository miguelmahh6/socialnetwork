import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegistrationModel } from '../models/registration.model';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { collection } from '@angular/fire/firestore';
import { PeopleService } from './people.service';


@Injectable({
    providedIn: 'root'
})
export class PublicationsService {



    constructor(private firestore: AngularFirestore, private peopleService: PeopleService) {
    }

    async create(data: any) {
        try {
            return await this.firestore.collection("publications").add(data);
        } catch (error) {
            return console.log("error creating : " + error);

        }
    }


    async update(idDocument: string, data: any) {
        try {
            return await this.firestore.collection("publications").doc(idDocument).set(data);
        } catch (error) {
            return console.log("error updating : " + error);

        }
    }

    async getAll() {
        try {
            return await this.firestore.collection("publications").snapshotChanges()

        } catch (error) {
            return console.log("error getting all : " + error);

        }
    }

    async getForHome() {
        try {

            return await this.firestore.collection("publications", ref => ref.where('emailUser', '!=', this.peopleService.userInSession.email)).snapshotChanges()


        } catch (error) {
            return console.log("error getting all : " + error);

        }
    }

    async getPublicationsForProfile(systemId: string) {
        try {

            return await this.firestore.collection("publications", ref => ref.where('idUser', '==', systemId)).snapshotChanges()

        } catch (error) {
            return console.log("error getting all : " + error);

        }
    }

}
