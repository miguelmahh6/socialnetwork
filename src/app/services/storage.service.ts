import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import { getStorage, ref, deleteObject } from "firebase/storage";

//firebase.initializeApp(environment.firebaseConfig);

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    storareRef: any;
    store: any;
    constructor() {


        let firebaseAppDefined = false

        setInterval(() => {
            if (!firebaseAppDefined) {
                if (firebase.app()) {
                    this.storareRef = firebase.app().storage().ref();

                    this.store = getStorage();
                    firebaseAppDefined = true
                }
            }
        }, 100)
    }


    async subirImagen(nombre: string, imgBase64: any) {

        try {
            let respuesta = await this.storareRef.child("profileImages/" + nombre).putString(imgBase64, 'data_url');
            return await respuesta.ref.getDownloadURL();
        } catch (err) {
            console.log(err);
            return null;
        }

    }

    async deleteImage(img: string) {
        // Create a reference to the file to delete
        const desertRef = ref(this.store, img);

        // Delete the file
        deleteObject(desertRef).then(() => {
            // File deleted successfully
        }).catch((error) => {
            // Uh-oh, an error occurred!
        });
    }
}