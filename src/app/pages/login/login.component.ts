import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from '../../services/auth.service';

import Swal from 'sweetalert2';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { PeopleService } from '../../services/people.service';
import { addDoc } from '@angular/fire/firestore';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();

  loginForm: FormGroup;


  constructor(private auth: AuthService, private spinner: NgxSpinnerService,
    private router: Router, private formBuilder: FormBuilder, private peopleService: PeopleService
  ) {

    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.router.navigateByUrl('/home')
    } else {
      if (localStorage.getItem('email') != null) {
        this.usuario.email = localStorage.getItem('email')!;
      }
    }



  }


  logIn() {
    if (this.loginForm.valid) {
      this.spinner.show();
      this.auth.login(this.loginForm.value)
        .subscribe(resp => {
          localStorage.setItem('email', this.loginForm.get('email')?.value);
          this.loadData();

          this.spinner.hide();
        }, (err) => {

          console.log(err.error.error.message);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: err.error.error.message
          });
          this.spinner.hide();

        });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Please',
        text: "fill all the inputs to sign in"

      });
    }
  }



  loadData() {
    this.peopleService.getAll().subscribe((data: any) => {
      //this.peopleService.allUsers = [];
      this.peopleService.allUsers = data;
      this.peopleService.userInSession = this.peopleService.getProfileBySystemId(this.auth.decodeJWT().user_id);
      localStorage.setItem("InformationUser", btoa(JSON.stringify(this.peopleService.userInSession)));
      this.router.navigateByUrl('/home');

    });
  }

}
