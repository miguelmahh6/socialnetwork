import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { PeopleService } from 'src/app/services/people.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';
import { profileImgPipe } from '../../shared/pipes/profileImg.pipe';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  userInformation: any = {};
  publications: any[] = [];
  imagenes: any = [];
  editForm: FormGroup;


  constructor(public auth: AuthService, private storageService: StorageService, private formBuilder: FormBuilder,
    private publicationService: PublicationsService, private peopleService: PeopleService,
    private spinner: NgxSpinnerService) {
    this.spinner.show();
    this.loadData();

    this.editForm = this.formBuilder.group({
      aboutMe: ["", [Validators.required]],
      bornDate: ["", [Validators.required]],
      firstName: ["", [Validators.required]],
      firstLastName: ["", [Validators.required]],
      secondName: ["", [Validators.required]],
      inRelationship: ["", [Validators.required]],
      secondLastName: ["", [Validators.required]]
    });



  }

  ngOnInit(): void {
    this.editForm.valueChanges.subscribe(
      controls => {
        this.userInformation.aboutMe = controls.aboutMe
        this.userInformation.bornDate = controls.bornDate
        this.userInformation.firstLastName = controls.firstLastName
        this.userInformation.firstName = controls.firstName
        this.userInformation.inRelationship = controls.inRelationship
        this.userInformation.secondLastName = controls.secondLastName
        this.userInformation.secondName = controls.secondName
      }
    )
  }



  loadData() {

    this.peopleService.getAll().subscribe((data: any) => {
      this.peopleService.allUsers = data;
      this.userInformation = this.peopleService.getProfileByIdSession();//

      this.editForm.patchValue({
        aboutMe: this.userInformation.aboutMe,
        bornDate: this.userInformation.bornDate,
        firstName: this.userInformation.firstName,
        firstLastName: this.userInformation.firstLastName,
        secondName: this.userInformation.secondName,
        inRelationship: this.userInformation.inRelationship,
        secondLastName: this.userInformation.secondLastName
      })
      this.publicationService.getPublicationsForProfile("").then((res: any) => {

        if (res) {
          res.subscribe((data: any) => {
            this.publications = [];

            data.forEach((element: any) => {
              var publicacion = element.payload.doc.data();
              publicacion.documentId = element.payload.doc.id;
              this.publications.push(publicacion);
            });

            console.log(this.publications);
            this.spinner.hide();

          });
        } else {
          this.spinner.hide();

        }


      })

    })





  }




  cargarImagen(event: any) {
    this.spinner.show();
    let archivo = event.target.files[0];
    let nombre = "profileImg";


    let reader = new FileReader();
    reader.readAsDataURL(archivo);
    reader.onloadend = () => {
      this.imagenes.push(reader.result);
      this.storageService.subirImagen(nombre + "_" + Date.now(), reader.result).then((urlImagen: any) => {

        //This condition, if i want to delete old img
        /* if (this.userInformation.profileImg) {
          this.storageService.deleteImage(this.userInformation.profileImg);

        } */
        this.userInformation.profileImg = urlImagen;


        //console.log(urlImagen);
        this.editarUsuario();
      });
    }
  }

  saveChanges() {
    if (this.editForm.valid) {
      const json = {
        ...this.editForm.value,
      }

      this.spinner.show()
      this.editarUsuario();



    } else {
      Swal.fire({
        icon: 'error',
        title: 'Please',
        text: "fill all the inputs to save"
      });
    }


  }

  editarUsuario() {
    this.peopleService.getAll().subscribe((data: any) => {
      this.peopleService.allUsers = data;
      //localStorage.removeItem("InformationUser");
      //localStorage.setItem("InformationUser", btoa(JSON.stringify(this.userInformation)));



      this.peopleService.editUser(this.buscarPosicion(), this.userInformation).subscribe((data: any) => {
        console.log(data);
        //  this.peopleService.getProfileByIdSession(); //getting profile Info to save it on localStorage


        this.spinner.hide();

        Swal.fire({
          icon: 'success',
          title: 'Information',
          text: "Saved",
          timer: 1000
        });
        // this.peopleService.userInSession = this.peopleService.getProfileBySystemId(this.auth.decodeJWT().user_id);

      })

    });
  }

  buscarPosicion() {
    var posicion: any;
    for (let index = 0; index < this.peopleService.allUsers.length; index++) {
      if (this.peopleService.allUsers[index].systemId == this.userInformation.systemId) {
        posicion = index;
      }
    }
    return posicion;
  }

}

