import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistrationModel } from 'src/app/models/registration.model';
import { PeopleService } from 'src/app/services/people.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  peopleFound: Array<RegistrationModel> = [];

  constructor(private router: Router, public peopleService: PeopleService, private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.spinner.show();
    this.activatedRoute.params.subscribe((params) => {


      this.searchPeople(params["name"]);

    })

  }

  goToProfile(person: RegistrationModel) {
    this.router.navigate(['profile', person.systemId]);
  }

  searchPeople(name: string) {
    this.peopleFound = [];
    this.peopleService.getAll().subscribe((data: any) => {
      this.peopleService.allUsers = [];
      this.peopleService.allUsers = data;

      name = name.toLowerCase();

      this.peopleService.allUsers.forEach(element => {
        var nameE = element.firstName + element.secondName + element.firstLastName + element.secondLastName;
        nameE = nameE.toLowerCase().trim();
        var nameA = name.toLowerCase().trim();
        if (nameE.indexOf(nameA) >= 0) {
          this.peopleFound.push(element)
        }
      });
      this.spinner.hide();
    });
  }
}
