import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { Observable } from 'rxjs';
import { PublicationModel } from '../../models/publication.model';
import { PeopleService } from '../../services/people.service';
import { profileImgPipe } from '../../shared/pipes/profileImg.pipe';
import { PublicationAnswerModel } from '../../models/publicationAnswer.model';
import { PublicationsService } from '../../services/publications.service';
import { PublicationAnswerCommentModel } from 'src/app/models/publicationAnswerComment.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    profileImgPipe, AngularFirestore
  ],
})
export class HomeComponent implements OnInit {


  publications: any[] = [];


  constructor(private profileImgPipe: profileImgPipe, private auth: AuthService,
    private firestore: AngularFirestore, private peopleService: PeopleService,
    private router: Router, private publicationService: PublicationsService, private spinner: NgxSpinnerService) {



  }

  ngOnInit() {
    this.spinner.show();
    this.getDataFromFireStore();

  }

  newPublication(publicationText: string) {
    var publication = new PublicationModel;
    publication.emailUser = this.peopleService.userInSession.email;
    publication.idUser = this.peopleService.userInSession.systemId;
    publication.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    publication.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    publication.post = { comments: [], msg: publicationText };



    this.publicationService.create(JSON.parse(JSON.stringify(publication)));

    /*  addDoc(collection(this.firestore, "publications"),
       JSON.parse(JSON.stringify(publication)))
       .then(docRef => {
         //console.log("Document written with ID: ", docRef.id);
       }) */

  }

  commentPublication(commentOfPublication: string, publication: any) {
    var publicationAnswer = new PublicationAnswerModel

    publicationAnswer.emailUser = this.peopleService.userInSession.email;
    publicationAnswer.idUser = this.peopleService.userInSession.systemId;
    publicationAnswer.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    publicationAnswer.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    publicationAnswer.comments = [];
    publicationAnswer.comment = commentOfPublication;

    console.log(publicationAnswer);
    publication.post.comments.push(publicationAnswer);
    this.publicationService.update(publication.documentId, JSON.parse(JSON.stringify(publication)));
  }


  replyComment(replyForComment: string, publication: any, commentOfPublication: any) {
    var myReplyForComment = new PublicationAnswerCommentModel

    myReplyForComment.emailUser = this.peopleService.userInSession.email;
    myReplyForComment.idUser = this.peopleService.userInSession.systemId;
    myReplyForComment.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    myReplyForComment.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    myReplyForComment.comment = replyForComment;

    commentOfPublication.comments.push(myReplyForComment)
    this.publicationService.update(publication.documentId, JSON.parse(JSON.stringify(publication)));
  }

  getDataFromFireStore() {
    this.publicationService.getForHome().then((res: any) => {
      if (res) {
        res.subscribe((data: any) => {
          this.publications = [];

          data.forEach((element: any) => {
            var publicacion = element.payload.doc.data();
            publicacion.documentId = element.payload.doc.id;
            this.publications.push(publicacion);
          });

          this.spinner.hide();
        });
      }else{
        this.spinner.hide();

      }
    })

  }

}

