import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from '../../services/auth.service';

import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { PeopleService } from '../../services/people.service';
import { RegistrationModel } from 'src/app/models/registration.model';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario!: UsuarioModel;
  recordarme = false;

  registForm: FormGroup;

  allUsers: Array<RegistrationModel> = [];


  constructor(private auth: AuthService, private formBuilder: FormBuilder, private router: Router
    , private peopleService: PeopleService) {

    if (this.auth.isAuthenticated()) {
      this.router.navigateByUrl('/home')
    }
    this.registForm = this.formBuilder.group({
      aboutMe: ["", [Validators.required]],
      bornDate: ["", [Validators.required]],
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
      firstName: ["", [Validators.required]],
      firstLastName: ["", [Validators.required]],
      secondName: ["", [Validators.required]],
      id: ["", [Validators.required]],
      inRelationship: [false, [Validators.required]],
      secondLastName: ["", [Validators.required]],
    });
  }

  ngOnInit() {


    // this.usuario = new UsuarioModel();
  }

  register() {

    
    if (this.registForm.valid) {

      this.auth.nuevoUsuario(this.registForm.value)
        .subscribe(resp => {


          const json = {
            ...this.registForm.value,
            systemId: resp.localId,
            profileImg: ""
          }

          this.peopleService.getAll().subscribe((data: any) => {
            this.allUsers = data;
            this.allUsers.push(json);

            this.peopleService.newUser(this.allUsers).subscribe((resp: any) => {
              localStorage.setItem('email', this.registForm.get('email')?.value);
              this.peopleService.getAll().subscribe((data: any) => {
                //this.peopleService.allUsers = [];
                this.peopleService.allUsers = data;
                this.peopleService.userInSession = this.peopleService.getProfileBySystemId(this.auth.decodeJWT().user_id);
               
                localStorage.setItem("InformationUser", btoa(JSON.stringify(this.peopleService.userInSession)));
                // this.peopleService.getProfileByIdSession(); //getting profile Info to save it on localStorage
              });

              this.router.navigateByUrl('/home');
            })

          });






        }, (err) => {
          Swal.fire({
            icon: 'error',
            title: 'Error al autenticar',
            text: err.error.error.message
          });
        });

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Please',
        text: "fill all the inputs to sign in"
      });
    }


  }


}
