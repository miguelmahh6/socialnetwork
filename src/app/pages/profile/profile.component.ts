import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { PeopleService } from '../../services/people.service';
import { NgxSpinnerService } from "ngx-spinner";

import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userInformation: any;
  systemId: string = "";
  imagenes: any;

  constructor(public auth: AuthService, private activatedRoute: ActivatedRoute,
    private peopleService: PeopleService,
    private spinner: NgxSpinnerService) {
    this.spinner.show();
    this.loadData();
  }

  ngOnInit(): void {

  }


  loadData() {
    this.activatedRoute.params.subscribe((params) => {

   
      this.systemId = params["systemId"];
      this.userInformation = this.peopleService.getProfileBySystemId(params["systemId"]);
    })




  }




}
