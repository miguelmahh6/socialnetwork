import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NavbarComponent } from './shared/components/navbar/navbar.component';

/* import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore'; */
import { AngularFireModule } from '@angular/fire/compat';

import { environment } from 'src/environments/environment';
import { profileImgPipe } from './shared/pipes/profileImg.pipe';
import { PublicationsComponent } from './shared/components/publications/publications.component';
import { ResultsComponent } from './pages/results/results.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';

@NgModule({
  declarations: [
    AppComponent,LoginComponent,HomeComponent,RegistroComponent, ProfileComponent, NavbarComponent,profileImgPipe, PublicationsComponent, ResultsComponent, MyProfileComponent
  ],
  imports: [
 /*    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()), */
    BrowserModule,AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    FormsModule,ReactiveFormsModule,NgxSpinnerModule,
    HttpClientModule,BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
