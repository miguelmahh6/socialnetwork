import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "profileImgPipe",
})
export class profileImgPipe implements PipeTransform {
    constructor() { }

    transform(urlImg: any) {

        if (urlImg == null || urlImg == "" || urlImg == '') {
            return "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
        } else {
            return urlImg

        }

    }
}
