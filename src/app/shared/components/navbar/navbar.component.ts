import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../../../services/people.service';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { RegistrationModel } from 'src/app/models/registration.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  peopleFound: Array<RegistrationModel> = [];

  constructor(public peopleService: PeopleService, public authService: AuthService, private router: Router) {


  }

  ngOnInit(): void {
  }


  logOut() {
    this.authService.logout();
    this.router.navigateByUrl("/login");
    this.peopleService.clearSession();
  }

 
  searchPeople(name: string) {

    this.router.navigate(['results', name])

  }
}
