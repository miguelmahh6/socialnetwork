import { Component, Input, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { PublicationModel } from "src/app/models/publication.model";
import { PublicationAnswerModel } from "src/app/models/publicationAnswer.model";
import { PublicationAnswerCommentModel } from "src/app/models/publicationAnswerComment.model";
import { AuthService } from "src/app/services/auth.service";
import { PeopleService } from "src/app/services/people.service";
import { PublicationsService } from "src/app/services/publications.service";
import { profileImgPipe } from "../../pipes/profileImg.pipe";

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css'],
  providers: [
    profileImgPipe, AngularFirestore
  ],
})
export class PublicationsComponent implements OnInit {

  publications: any[] = [];
  @Input() from = "";
  @Input() systemId = "";
  timeLeft: number = 2;
  interval: any;
  isLike = false;

  constructor(private profileImgPipe: profileImgPipe, private auth: AuthService, private spinner: NgxSpinnerService,
    private firestore: AngularFirestore, private peopleService: PeopleService,
    private router: Router, private publicationService: PublicationsService) {

  }



  ngOnInit() {
    this.getDataFromFireStore();

  }


  likeDislike() {
    if (this.isLike) {
      this.isLike = false;
    } else {
      this.isLike = true;

    }
  }

  newPublication(publicationText: string) {
    var publication = new PublicationModel;
    publication.emailUser = this.peopleService.userInSession.email;
    publication.idUser = this.peopleService.userInSession.systemId;
    publication.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    publication.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    publication.post = { comments: [], msg: publicationText };



    this.publicationService.create(JSON.parse(JSON.stringify(publication)));

    /*  addDoc(collection(this.firestore, "publications"),
       JSON.parse(JSON.stringify(publication)))
       .then(docRef => {
         //console.log("Document written with ID: ", docRef.id);
       }) */

  }

  commentPublication(commentOfPublication: string, publication: any) {
    var publicationAnswer = new PublicationAnswerModel

    publicationAnswer.emailUser = this.peopleService.userInSession.email;
    publicationAnswer.idUser = this.peopleService.userInSession.systemId;
    publicationAnswer.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    publicationAnswer.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    publicationAnswer.comments = [];
    publicationAnswer.comment = commentOfPublication;

    publication.post.comments.push(publicationAnswer);
    this.publicationService.update(publication.documentId, JSON.parse(JSON.stringify(publication)));
  }


  replyComment(replyForComment: string, publication: any, commentOfPublication: any) {
    var myReplyForComment = new PublicationAnswerCommentModel

    myReplyForComment.emailUser = this.peopleService.userInSession.email;
    myReplyForComment.idUser = this.peopleService.userInSession.systemId;
    myReplyForComment.nombres = this.peopleService.userInSession.firstName + " " + this.peopleService.userInSession.firstLastName;
    myReplyForComment.profileImg = this.profileImgPipe.transform(this.peopleService.userInSession.profileImg);
    myReplyForComment.comment = replyForComment;

    commentOfPublication.comments.push(myReplyForComment)
    this.publicationService.update(publication.documentId, JSON.parse(JSON.stringify(publication)));
  }

  getDataFromFireStore() {

    //if its from home, search home, if not search profile
    if (this.from == "home") {
      this.publicationService.getForHome().then((res: any) => {
        if (res) {
          res.subscribe((data: any) => {
            this.publications = [];

            data.forEach((element: any) => {
              var publicacion = element.payload.doc.data();
              publicacion.documentId = element.payload.doc.id;
              this.publications.push(publicacion);
            });

            this.spinner.hide();

          });
        } else {
          this.spinner.hide();

        }

      })
    } else {
      var id = "";
      if (this.from == "myProfile") {
        id = this.peopleService.userInSession.systemId;
      } else {
        id = this.systemId;
      }

      //this.systemId = this.peopleService.userInSession.systemId;
      this.publicationService.getPublicationsForProfile(id).then((res: any) => {
        if (res) {
          res.subscribe((data: any) => {
            this.publications = [];

            data.forEach((element: any) => {
              var publicacion = element.payload.doc.data();
              publicacion.documentId = element.payload.doc.id;
              this.publications.push(publicacion);
            });

            this.spinner.hide();

          });
        } else {
          this.spinner.hide();

        }

      })
    }

  }


}
