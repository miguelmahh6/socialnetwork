

export class RegistrationModel {

    email!: string;
    bornDate!: string;
    aboutMe!: string;
    firstName!: string;
    firstLastName!: string;
    id!: string;
    inRelationship!: boolean;
    profileImg!: string;
    secondName!: string;
    secondLastName!: string;
    systemId!: string;



}

