// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCmflSZNaWCkpR1bn36q5zkeI2W4b5jhjY",
    authDomain: "tpbd-80184.firebaseapp.com",
    databaseURL: "https://tpbd-80184-default-rtdb.firebaseio.com",
    projectId: "tpbd-80184",
    storageBucket: "tpbd-80184.appspot.com",
    messagingSenderId: "99260179955",
    appId: "1:99260179955:web:1b2119577b7d8e12a6de86"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
